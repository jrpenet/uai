const path = require('path')

module.exports = {
    entry: {
        index: ['babel-polyfill', './src/index.js'],
        cardapio: ['babel-polyfill', './src/cardapio.js'],
        batatas: ['babel-polyfill', './src/batatas.js'],
        coxinhas: ['babel-polyfill', './src/coxinhas.js'],
        salgada: ['babel-polyfill', './src/salgada.js'],
        conftemp: ['babel-polyfill', './src/conftemp.js'],
        beverage: ['babel-polyfill', './src/beverage.js'],
        confirma: ['babel-polyfill', './src/confirma.js'],
        txentrega: ['babel-polyfill', './src/txentrega.js'],
        forms: ['babel-polyfill', './src/forms.js'],
        closed: ['babel-polyfill', './src/closed.js'],
        confcli: ['babel-polyfill', './src/confcli.js'],
        enviapedidos: ['babel-polyfill', './src/enviapedidos.js']
    },
    output: {
        path: path.resolve(__dirname, 'public/scripts'),
        filename: '[name]-bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/scripts/'
    },
    devtool: 'source-map'
}