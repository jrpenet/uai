import {gettemp} from './temp'
import {criaPedidos} from './pedidos'
// import { addCardapioMolho, addCardapioMolhoEspetinho, addCardapioRecheios } from './molhosrecheios'

if(gettemp().length < 1){
    location.assign('./cardapio.html')
}

//mostra o nome do pedido na pagina
const mostraNomeDoPedido = document.querySelector('#nomePedido')
mostraNomeDoPedido.textContent = gettemp()[0].produto

//mostra a imagem do pedido
const showOrder = document.querySelector('#imagemApr')
showOrder.setAttribute('src', gettemp()[0].foto)

//pega o q foi digitado em obs
const pegaObs = document.querySelector('#insereObs')

//botao que esta na page
const botaoConfirmaInsere = document.querySelector('#confirmaInsere')

//configuração para aparecer as opções de pastel, de acordo com o escolhido
if(gettemp()[0].produto === 'Pastel Tradicional'){
    const pastelTrad = document.querySelector('#pastelTradicional')
    pastelTrad.removeAttribute('style', 'display: none;')
} 

if(gettemp()[0].tipo !== 'pastel6' && gettemp()[0].tipo !== 'petisco' && gettemp()[0].tipo !== 'caldinho'){
    const pastelTrad = document.querySelector('#pastelPremium')
    pastelTrad.removeAttribute('style', 'display: none;')
}

if(gettemp()[0].produto === 'Batata Palito - P' || gettemp()[0].produto === 'Batata Palito - M' || gettemp()[0].produto === 'Batata Palito - G' || gettemp()[0].produto === 'Batata Palito - GG'){
    const opcionalBatata = document.querySelector('#opcBatata')
    opcionalBatata.removeAttribute('style', 'display: none;')
}

//efeitos para os sabores amarelo e vermelho
const arrayOpcs = document.querySelectorAll('.checado3')
arrayOpcs.forEach((item) => {
    item.addEventListener('change', () => {
        if(item.checked == true){
            for(let i = 0; i < arrayOpcs.length; i++){
                if(arrayOpcs[i].checked == true){
                    const pai = arrayOpcs[i].parentNode
                    pai.setAttribute('style', 'color: yellow;')
                }else{
                    const pai = arrayOpcs[i].parentNode
                    pai.setAttribute('style', 'color: red;')
                }
            }
        }

    })
})

botaoConfirmaInsere.addEventListener('click', (e) => {
    e.preventDefault()

    if(gettemp()[0].produto === 'Pastel Tradicional'){
        
        let boxes = document.querySelectorAll('.checado')
        let rs = ''
    
        for(let i = 0; i < 5; i++){
            if(boxes[i].checked === true){
                rs += boxes[i].value
            }
        }

        if(rs == '') {
            alert('Escolha um sabor')
        } else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Sabor: ' + rs + ' - Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
            location.assign('./confirma.html')
        }

    }else if(gettemp()[0].tipo == 'pastel7' || gettemp()[0].tipo == 'pastel8' || gettemp()[0].tipo == 'pastel85' || gettemp()[0].tipo == 'pastel9' || gettemp()[0].tipo == 'pastel10'){

        let boxes = document.querySelectorAll('.checado2')
        let rs = ''

        for(let i = 0; i < 6; i++){
            if(boxes[i].checked === true){
                rs += boxes[i].value + ', '
            }
        }

        //adicionais
        const adicionaisPastel = document.querySelectorAll('.checadoAdd')
        let escolha = ''

        for(let i = 0; i < adicionaisPastel.length; i++){
            if(adicionaisPastel[i].checked == true){
                escolha += adicionaisPastel[i].value + ', '
            }
        }
        
        if(gettemp()[0].tipo == undefined){
            alert('Escolha um sabor')
            location.assign('./index.html')
        }else if(gettemp()[0].tipo == 'pastel7'){
            if(escolha == 'Ovo de Codorna, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 7.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 8, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Ovo de Codorna, Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 8.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else{
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + ' - Obs: ' + pegaObs.value, 7, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }
        }else if(gettemp()[0].tipo == 'pastel8'){
            if(escolha == 'Ovo de Codorna, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 8.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 9, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Ovo de Codorna, Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 9.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else{
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + ' - Obs: ' + pegaObs.value, 8, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }
        }else if(gettemp()[0].tipo == 'pastel85'){
            if(escolha == 'Ovo de Codorna, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 9, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 9.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Ovo de Codorna, Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 10, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else{
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + ' - Obs: ' + pegaObs.value, 8.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }
        }else if(gettemp()[0].tipo == 'pastel9'){
            if(escolha == 'Ovo de Codorna, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 9.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 10, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Ovo de Codorna, Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 10.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else{
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + ' - Obs: ' + pegaObs.value, 9, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }
        }else if(gettemp()[0].tipo == 'pastel10'){
            if(escolha == 'Ovo de Codorna, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 10.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 11, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else if(escolha == 'Ovo de Codorna, Catupiry, '){
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + '. Adicional de: ' + escolha + ' - Obs: ' + pegaObs.value, 11.5, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }else{
                criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + ' - Obs: ' + pegaObs.value, 10, gettemp()[0].tipo)
                location.assign('./confirma.html')
            }
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + '. Comp.: ' + rs + ' - Obs: ' + pegaObs.value, 999, gettemp()[0].tipo)
            location.assign('./confirma.html')
        }

    }else if(gettemp()[0].produto === 'Batata Palito - P' || gettemp()[0].produto === 'Batata Palito - M' || gettemp()[0].produto === 'Batata Palito - G' || gettemp()[0].produto === 'Batata Palito - GG'){
        const extra = document.querySelector('#incluiBatata')
        if(extra.checked == true){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' + Adicional Cheddar e Bacon. Obs: ' + pegaObs.value, gettemp()[0].preco + 2.5, gettemp()[0].tipo)
            location.assign('./confirma.html')
        }else{
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
            location.assign('./confirma.html')
        }
        
    }else{
        criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo)
        location.assign('./confirma.html')
    }

    
    
})