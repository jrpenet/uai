import {getPedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'
import { apagatemp } from './temp'

//refresh automatico pra evitar bugs

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })();

cartTable()
insereCart()
rodapeCart()
apagatemp()

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()

    const semTx = getPedidos().filter((x) => x.taxa === 'taxa').length

    //console.log(semTx)

    if(semTx < 1){
        location.assign('./txentrega.html')
        
    }else{

        const valor = getPedidos().filter((x) => x.taxa !== 'taxa').map((e) => e.subt).reduce((a, b) => {
            return a + b
        }, 0)
    
        const retiradaLa = getPedidos().filter((x) => x.taxa === 'taxa')[0].produto === 'Retirar na Loja'

        if(getPedidos().map((x)=> x.taxa).includes('taxa') && valor >= 10 || getPedidos().map((x)=> x.taxa).includes('taxa') && retiradaLa){
            location.assign('./forms.html')
            //console.log('forms')
        }if(valor < 10 && !retiradaLa){
            alert('Fazemos entrega para valores acima de R$ 10,00 sem a taxa de entrega. Continue comprando, clique em Comprar Mais')
        }
    }
})