let clientes = []

const loadClientes = function(){
    const clientesJSON = localStorage.getItem('clientes')
    
    if(clientesJSON !== null){
        return JSON.parse(clientesJSON)
    } else {
        return []
    }
}

const saveClientes = function(){
    localStorage.setItem('clientes', JSON.stringify(clientes))
}

//expose orders from module
const getClientes = () => clientes

//funcao para add clientes
const insereClientes = (nome, end, num, cep, cel, pRef, pg, troco, obs) => {
    clientes.unshift(nome, end, num, cep, cel, pRef, pg, troco, obs)
    saveClientes()
}

//funcao para limpar registro do cliente
const removeClientes = () => {
    localStorage.clear()
}

const mudaDados = (pgNEW, trocoNEW, obsNEW) => {
    clientes.splice(5,3)
    clientes.push(pgNEW, trocoNEW, obsNEW)
    saveClientes()
}

clientes = loadClientes()

export {getClientes, insereClientes, removeClientes, saveClientes, mudaDados}