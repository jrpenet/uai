import { criatemp } from './temp'

//gera o menu na tela 

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Pastel de Frango',
    descricao: '',
    preco: 6,
    foto: './images/pasteltrad.jpeg',
    tipo: 'pastel6'
},{
    nomeHamburguer: 'Pastel de Carne',
    descricao: '',
    preco: 6,
    foto: './images/pasteltrad.jpeg',
    tipo: 'pastel6' 
},{
    nomeHamburguer: 'Pastel de Queijo',
    descricao: '',
    preco: 6,
    foto: './images/pasteltrad.jpeg',
    tipo: 'pastel6'
},{
    nomeHamburguer: 'Pastel de Calabresa',
    descricao: '',
    preco: 6,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel6'
},{
    nomeHamburguer: 'Pastel de Calabresa e Queijo',
    descricao: '',
    preco: 7,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel7'
},{
    nomeHamburguer: 'Pastel de Frango e Queijo',
    descricao: '',
    preco: 7,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel7'
},{
    nomeHamburguer: 'Pastel de Carne e Queijo',
    descricao: '',
    preco: 7,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel7'
},{
    nomeHamburguer: 'Pastel de Queijo e Presunto',
    descricao: '',
    preco: 7,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel7'
},{
    nomeHamburguer: 'Pastel de Bacon e Queijo',
    descricao: '',
    preco: 7,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel7'
},{
    nomeHamburguer: 'Pastel de Charque e Queijo',
    descricao: '',
    preco: 7,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel7'
},{
    nomeHamburguer: 'Pastel de Frango, Queijo e Bacon',
    descricao: '',
    preco: 8,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel8'
},{
    nomeHamburguer: 'Pastel de Carne, Cheddar e Presunto',
    descricao: '',
    preco: 8,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel8'
},{
    nomeHamburguer: 'Pastel de Frango, Catupiry e Calabresa',
    descricao: '',
    preco: 8,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel8'
},{
    nomeHamburguer: 'Pastel de Carne, Catupiry e Bacon',
    descricao: '',
    preco: 8,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel8'
},{
    nomeHamburguer: 'Pastel de Charque, Carne e Cheddar',
    descricao: '',
    preco: 8.5,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel85'
},{
    nomeHamburguer: 'Pastel de Bacon, Frango, Catupiry e Calabresa',
    descricao: '',
    preco: 9,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel9'
},{
    nomeHamburguer: 'Pastel de Charque, Carne, Catupiry e Calabresa',
    descricao: '',
    preco: 9,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel9'
},{
    nomeHamburguer: 'Pastel de Frango, Calabresa, Charque e Queijo',
    descricao: '',
    preco: 9,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel9'
},{
    nomeHamburguer: 'Pastel de Charque, Frango, Presunto e Queijo',
    descricao: '',
    preco: 9,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel9'
},{
    nomeHamburguer: 'Pastel de Frango, Charque, Cheddar, Presunto e Bacon',
    descricao: '',
    preco: 10,
    foto: './images/pastelao.jpeg',
    tipo: 'pastel10'
}, {
    nomeHamburguer: 'Batata Palito - P',
    descricao: 'Você pode adicionar Cheddar e Bacon',
    preco: 5,
    foto: './images/palitos.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Batata Palito - M',
    descricao: 'Você pode adicionar Cheddar e Bacon',
    preco: 7,
    foto: './images/palitos.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Batata Palito - G',
    descricao: 'Você pode adicionar Cheddar e Bacon',
    preco: 8,
    foto: './images/palitos.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Batata Palito - GG',
    descricao: 'Você pode adicionar Cheddar e Bacon',
    preco: 12,
    foto: './images/palitos.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Chips - P',
    descricao: '',
    preco: 3,
    foto: './images/chips.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Chips - M',
    descricao: '',
    preco: 5,
    foto: './images/chips.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Chips - M',
    descricao: '',
    preco: 7,
    foto: './images/chips.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Chips - GG',
    descricao: '',
    preco: 10,
    foto: './images/chips.jpg',
    tipo: 'caldinho'
}, {
    nomeHamburguer: 'Coxinha de Frango',
    descricao: '',
    preco: .8,
    foto: './images/frango.jpeg',
    tipo: 'petisco'
}, {
    nomeHamburguer: 'Coxinha de Frango com Catupiry',
    descricao: '',
    preco: .8,
    foto: './images/frangoc.jpeg',
    tipo: 'petisco'
}]

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const opt4 = document.createElement('option')
    const opt5 = document.createElement('option')
    const opt6 = document.createElement('option')
    const opt7 = document.createElement('option')
    const opt8 = document.createElement('option')
    const opt9 = document.createElement('option')
    const opt10 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    opt4.setAttribute('value', '4')
    opt4.textContent = '4'
    opt5.setAttribute('value', '5')
    opt5.textContent = '5'
    opt6.setAttribute('value', '6')
    opt6.textContent = '6'
    opt7.setAttribute('value', '7')
    opt7.textContent = '7'
    opt8.setAttribute('value', '8')
    opt8.textContent = '8'
    opt9.setAttribute('value', '9')
    opt9.textContent = '9'
    opt10.setAttribute('value', '10')
    opt10.textContent = '10'
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    select.appendChild(opt4)
    select.appendChild(opt5)
    select.appendChild(opt6)
    select.appendChild(opt7)
    select.appendChild(opt8)
    select.appendChild(opt9)
    select.appendChild(opt10)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')        
    })

    imagem.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        const img = lanche.foto
        criatemp(qt, prodt, price, tp, '', img)
        location.assign('./conftemp.html')
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        const img = lanche.foto
        criatemp(qt, prodt, price, tp, '', img)
        location.assign('./conftemp.html')
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo.includes('pastel')).forEach((lanche) => {
        document.querySelector('#docTable').appendChild(addElements(lanche))  
    })
}

const addCardapioTiaGil = () => {
    cardapio.filter((x) => x.tipo === 'caldinho').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable2').appendChild(hrEl)
        document.querySelector('#docTable2').appendChild(addElements(lanche))
        
    })
}

const addCardapioDoce = () => {
    cardapio.filter((x) => x.tipo === 'petisco').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable3').appendChild(hrEl)
        document.querySelector('#docTable3').appendChild(addElements(lanche))
        
    })
}

const addCardapioSalgada = () => {
    cardapio.filter((x) => x.tipo === 'salgada').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docTable4').appendChild(hrEl)
        document.querySelector('#docTable4').appendChild(addElements(lanche))
        
    })
}


export {addCardapio, addCardapioTiaGil, addCardapioDoce, addCardapioSalgada, addElements}