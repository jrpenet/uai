import {criaPedidos} from './pedidos'

//tabela do menu

const addItemTbl = function (){
    const tabela = document.createElement('table')
    document.querySelector('#docBebidas').appendChild(tabela)
    
    //header da tabela
    
    const tdQT = document.createElement('th')
    tdQT.textContent = 'QTD'
    document.querySelector('table').appendChild(tdQT)
    
    const tdItem2 = document.createElement('th')
    tdItem2.textContent = 'ITEM'
    document.querySelector('table').appendChild(tdItem2)
    
    const tdPRC = document.createElement('th')
    tdPRC.textContent = 'PREÇO'
    document.querySelector('table').appendChild(tdPRC)
    
    const tdpede = document.createElement('th')
    tdpede.textContent = 'PEDIR'
    document.querySelector('table').appendChild(tdpede)
}

//gera o menu na tabela

const listaDeBebidas = [{
    nomeDaBebida: 'Fanta 1.5L',
    valor: 7
}, {
    nomeDaBebida: 'Refri 1L',
    valor: 4
}, {
    nomeDaBebida: 'Refri 200ml',
    valor: 2
}, {
    nomeDaBebida: 'Coca-Cola 1L', 
    valor: 6.5
}, {
    nomeDaBebida: 'Guaraná 1L',
    valor: 5
}, {
    nomeDaBebida: 'Guaraná 200ml',
    valor: 2
}, {
    nomeDaBebida: 'Suco Vale 450ml',
    valor: 3.5
}]

const addElementsBeverage = (bebida) => {
    const bebidaElements = document.createElement('tr')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const opt4 = document.createElement('option')
    const opt5 = document.createElement('option')
    const opt6 = document.createElement('option')
    const opt7 = document.createElement('option')
    const opt8 = document.createElement('option')
    const opt9 = document.createElement('option')
    const opt10 = document.createElement('option')
    const opt11 = document.createElement('option')
    const opt12 = document.createElement('option')
    const opt13 = document.createElement('option')
    const opt14 = document.createElement('option')
    const opt15 = document.createElement('option')
    const opt16 = document.createElement('option')
    const opt17 = document.createElement('option')
    const opt18 = document.createElement('option')
    const opt19 = document.createElement('option')
    const opt20 = document.createElement('option')
    const tdEl = document.createElement('td')
    const tdEl2 = document.createElement('td')
    const tdEl3 = document.createElement('td')
    const tdEl4 = document.createElement('td')
    const button = document.createElement('button')


    select.setAttribute('id', bebida.nomeDaBebida.replace(/[ 1-]/g, '').replace(/[ãá]/g, 'a').toLowerCase())
    bebidaElements.appendChild(tdEl)
    tdEl.appendChild(select)
    opt.setAttribute('value', '1')
    opt.textContent = '1'
    opt2.setAttribute('value', '2')
    opt2.textContent = '2'
    opt3.setAttribute('value', '3')
    opt3.textContent = '3'
    opt4.setAttribute('value', '4')
    opt4.textContent = '4'
    opt5.setAttribute('value', '5')
    opt5.textContent = '5'
    opt6.setAttribute('value', '6')
    opt6.textContent = '6'
    opt7.setAttribute('value', '7')
    opt7.textContent = '7'
    opt8.setAttribute('value', '8')
    opt8.textContent = '8'
    opt9.setAttribute('value', '9')
    opt9.textContent = '9'
    opt10.setAttribute('value', '10')
    opt10.textContent = '10'
    opt11.setAttribute('value', '11')
    opt11.textContent = '11'
    opt12.setAttribute('value', '12')
    opt12.textContent = '12'
    opt13.setAttribute('value', '13')
    opt13.textContent = '13'
    opt14.setAttribute('value', '14')
    opt14.textContent = '14'
    opt15.setAttribute('value', '15')
    opt15.textContent = '15'
    opt16.setAttribute('value', '16')
    opt16.textContent = '16'
    opt17.setAttribute('value', '17')
    opt17.textContent = '17'
    opt18.setAttribute('value', '18')
    opt18.textContent = '18'
    opt19.setAttribute('value', '19')
    opt19.textContent = '19'
    opt20.setAttribute('value', '20')
    opt20.textContent = '20'
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    select.appendChild(opt4)
    select.appendChild(opt5)
    select.appendChild(opt6)
    select.appendChild(opt7)
    select.appendChild(opt8)
    select.appendChild(opt9)
    select.appendChild(opt10)

    if(bebida.nomeDaBebida === 'Devassa latão' || bebida.nomeDaBebida === 'Budweiser 550ml' || bebida.nomeDaBebida === 'Stella 550ml' || bebida.nomeDaBebida === 'Petra lata' || bebida.nomeDaBebida === 'Itaipava Latão'){
        select.appendChild(opt11)
        select.appendChild(opt12)
        select.appendChild(opt13)
        select.appendChild(opt14)
        select.appendChild(opt15)
        select.appendChild(opt16)
        select.appendChild(opt17)
        select.appendChild(opt18)
        select.appendChild(opt19)
        select.appendChild(opt20)
    } 
    

    tdEl2.textContent = bebida.nomeDaBebida
    bebidaElements.appendChild(tdEl2)

    tdEl3.setAttribute('class', 'valorDaBebida')
    tdEl3.textContent = 'R$ ' + bebida.valor.toFixed(2).replace('.', ',')
    bebidaElements.appendChild(tdEl3)

    bebidaElements.appendChild(tdEl4)
    button.textContent = 'PEDIR'
    tdEl4.appendChild(button)
    
    button.addEventListener('click', (e) => {
        const quant = parseFloat(select.value)
        const prd = bebida.nomeDaBebida
        const prc = bebida.valor
        criaPedidos(quant, prd, prc)
        location.assign('././txentrega.html')
    })
   
    return bebidaElements
}

const addMenuBebidas = () => {
    listaDeBebidas.forEach((bebida) => {
        document.querySelector('table').appendChild(addElementsBeverage(bebida))
    })
}

export {addItemTbl, addMenuBebidas, addElementsBeverage}