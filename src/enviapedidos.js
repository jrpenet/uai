import {getPedidos} from './pedidos'
import {getClientes} from './cliente'

if(getPedidos().map((x)=> x.taxa).includes('taxa')){
   
}else{
   location.assign('./txentrega.html')
}

const usuarioNome = document.querySelector('#usuario')
usuarioNome.textContent = getClientes()[0].toUpperCase()

const valor = getPedidos().map((e) => e.subt).reduce((a, b) => {
    return a + b
}, 0)

document.querySelector('.btnFim').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign(`https://wa.me/558198005502?text=%2ANome%2A%3A%20${getClientes()[0]}%0A%0A%2ACEP%2A%3A%20${getClientes()[3]}%0A%0A%2AEndere%C3%A7o%2A%3A%20${getClientes()[1]}%2C%20${getClientes()[2]}%0A%0A%2ABairro%2A%3A%20${getPedidos().map((z) => z.nomeDoBairro).join('')}%0A%0A%2ACelular%2A%3A%20${getClientes()[4]}%0A%0A%2APonto%20de%20refer%C3%AAncia%2A%3A%20${getClientes()[5]}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getClientes()[6]}%0A%0A%2ATroco%2A%3A%20${getClientes()[7]}%0A%0A%2AObserva%C3%A7%C3%A3o%2A%3A%20${getClientes()[8]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, x.produto, 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AValor%20total%2A%3A%20R$${valor.toFixed(2).replace('.', ',')}`)
})